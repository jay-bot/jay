const express = require('express')

const bodyParser = require("body-parser");

const app = express()

//const Egg = require('./core/libs/eggs').Egg
//const when = require('./core/libs/eggs').when

const axios = require("axios")


const txt = require('./core/libs/text')

const Bot = require('./core/models/Bot').Bot
const botName = process.env.BOT_NAME || "root"

const port = process.env.PORT || 9090

//app.use(express.static('public'))
app.use(express.static('public'))
app.use(express.json())

// for Slack
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


let bot = Bot.of({
  userName: botName,
})

const loadIntegrations = require(`./integrations/integrations.js`)
loadIntegrations(app, bot)

app.listen(port, () => {
  console.log(`👋 Jay the 🤖 is listening on port ${port} 😃`)
  console.log(`🌍 http://localhost:${port}`)
})