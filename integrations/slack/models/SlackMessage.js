const Egg = require('../../../core/libs/eggs').Egg

/*
{ token: '4xosPpWv2b8t0qmDIxv9c91q',
  team_id: 'T9J85JS76',
  team_domain: 'botsgarden',
  service_id: '560713062420',
  channel_id: 'C9HN1K4RX',
  channel_name: 'general',
  timestamp: '1551156564.001300',
  user_id: 'U9HK2470Q',
  user_name: 'k33g',
  text: '@babs yo',
  trigger_word: '@babs' }
*/

class SlackMessage extends Egg {
  
  text() {
    return this.value().text
  }

  token() {
    return this.value().token
  }  

  triggerWord() {
    return this.value().trigger_word
  }

  channelId() {
    return this.value().channel_id
  }

  channelName() {
    return this.value().channel_name
  }

  userId() {
    return this.value().user_id || "???"
  }
  userName() {
    return this.value().user_name || "John Doe"
  }

}

module.exports = {
  SlackMessage: SlackMessage
}