// Slack 🚧
console.log("🤖 > Slack events management ✅")

const DictionnaryFromYAML = require('../../core/models/DictionnaryFromYAML').DictionnaryFromYAML
const rules_path = `./rules/${process.env.RULES_PATH}` || "./rules/demo"
// Load dictionnary(ies)
const yamlDictionnary = DictionnaryFromYAML.of(`${rules_path}/.rules.yml`)

const nlpMethod = yamlDictionnary.getNLP() 
  ? yamlDictionnary.getNLP().method 
  : "jaroWinkler" // default value
/* Possible values:
- jaroWinkler
- levenshtein
- diceCoefficient
*/

const similarityTrigger = yamlDictionnary.getNLP()
  ? yamlDictionnary.getNLP().similarityTrigger
  : 0.8

module.exports =  ({bot, event}) =>  {

  // ⚠️ it's important
  let channel = event.channelName()

  let options = {
    BOT_NAME: bot.userName(), 
    USER_NAME: event.userName(), 
    HELLO: "👋 hello 🌍 world",
    CURRENT_PATH: rules_path,
    QUESTION: event.text(),
    CHANNEL: channel
  }

  let disparities = []
  let answers = []

  bot.hearing({content: event.text()}).when({

    Notification: text => {

      options.notification=true

      let search = text.search({
        dictionnary: yamlDictionnary, 
        bot: bot, 
        event: event, 
        options: options
      })

      if(event.text().includes("%CMD")) {
        console.log("👋 using command mode")
        search["startsWith"]().when({
          Disparities: disparities => {},
          Similarities: similarities => {
            if(similarities.first().scriptResult) {
              answers.push(similarities.first().scriptResult)
            } else {
              answers.push(similarities.first().answer) 
            }
          }
        })
      } else {

        // Jaro-Winkler
        // best similarity -> 1
        // worst similarity -> 0
        search[nlpMethod](similarityTrigger).when({
          Disparities: disparities => {},
          Similarities: similarities => {
            if(similarities.first().scriptResult) {
              answers.push(similarities.first().scriptResult)
            } else {
              answers.push(similarities.first().answer) 
            }
          }
        })

      }

      // answering ...
      answers.length>0 
        ? bot.sendToSlack(channel, answers.join("\n"))
        : bot.sendToSlack(channel, `😢 sorry @${event.userName()}, can you repeat please?`)
        //TODO: read the message from yaml file

    },
    NotNotification: text => { 
      // there is a message, but it's not for the bot
      options.notification=false

      let search = text.search({
        dictionnary: yamlDictionnary, 
        bot: bot, 
        event: event, 
        options: options
      })

      search[nlpMethod](similarityTrigger).when({
        Disparities: disparities => {},
        Similarities: similarities => {
          if(similarities.first().scriptResult) {
            answers.push(similarities.first().scriptResult)
          } else {
            answers.push(similarities.first().answer) 
          }
        }
      })
      // answering ...
      answers.length>0 
        ? bot.sendToSlack(channel, answers.join("\n"))
        : null // don't add message: no notification

      //console.log(text)
    }
  })

}
