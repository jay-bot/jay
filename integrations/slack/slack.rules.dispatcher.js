console.log("🤖 > loading 🌼 Slack rules")

const slack_events_path = process.env.REST_EVENTS_PATH || "./"
const whenSlackEvent = require(`${slack_events_path}/when.slack.event.js`)

module.exports =  ({bot, slackEvent}) =>  {
  
  slackEvent.when({
    SlackMessage: payload => { 
      whenSlackEvent({bot: bot, event: slackEvent})
      bot.httpStatus(200)
    },      
    _: () => { // otherwise
      let message = "😡 ouch something went bad"
      console.log(message)
      bot.httpResponse(message)
    }
  })
  
}
