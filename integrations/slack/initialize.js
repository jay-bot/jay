const axios = require("axios")
console.log("🤖 > 🌼 Loading Slack integration")

module.exports =  (app, bot) =>  {

  let SlackMessage = require(`./models/SlackMessage`).SlackMessage
  const slackRulesDispatcher = require(`./slack.rules.dispatcher.js`)

  /* 
  ## Slack Configuration

  ```
  SLACK_HOOKS_BASE_URL="https://hooks.slack.com/services/" \
  SLACK_INCOMING_TOKEN="T9J85JS76/BGJ5FKHCN/KCgTHSmsEfLBFuQxEWl6uWlq" \
  SLACK_CHANNELS="general,random" \
  SLACK_OUTGOING_TOKENS="4xosPpWv2b8t0qmDIxv9c91q,Fhkkr57A1bkQasY2jWQeHr3q" \
  ```
  */

  let slackHooksBaseUrl = process.env.SLACK_HOOKS_BASE_URL || null
  let slackChannels = process.env.SLACK_CHANNELS.split(",")
  let slackIncomingToken = process.env.SLACK_INCOMING_TOKEN
  let slackOutgoingTokens = process.env.SLACK_OUTGOING_TOKENS.split(",")

  let slackWebHook = slackHooksBaseUrl + slackIncomingToken

  console.log("🌼 Slack webhook:", slackWebHook)

  bot.setProperties({
    slackWebHook: slackWebHook
  })

  bot.setMethods({
    sendToSlack: function(channel, message) {
      let webHook = this.slackWebHook

      return axios({
        method: "POST",
        url: webHook,
        headers: {"Content-Type": "application/json"},
        data: JSON.stringify({
          "channel": `#${channel}`,
          "username": `${this.userName()}`,
          "text": message
        })
      })
      /*
      .then(response => response)
      .catch(err => console.log(`😡 Context.send`, err))
      */    
    }
  })

  /*
  ### Slack
  */
  app.post('/slack/knock-knock', (req, res) => {

    let message = SlackMessage.of(req.body)
    console.log("🌻 slackt knock knock from:", message.channelName())

    let slackBotToken = slackOutgoingTokens[slackChannels.indexOf(message.channelName())]
        
    bot.setHttpContext({
      request: req,
      response: res
    })

    if(message.token() == slackBotToken) {
      slackRulesDispatcher({
        bot: bot,
        slackEvent: message
      })

      res.status(200).end()

    } else {
      console.log("😡 🌼 Slack knock knock with bad token")
      res.status(200).end()
    }

  })

  //TODO: add token? or not
  //TODO: to be secured
  app.post('/slack/message', (req, res) => {
    console.log("🐡 [Slack]Message from the API", req.body)
    bot.sendToSlack(req.body.channel, req.body.text)
    res.status(200).end()
  })

}



