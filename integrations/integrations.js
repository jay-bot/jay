// Load Integrations
const fs = require('fs')
const rules_path = `./rules/${process.env.RULES_PATH}` || "./rules/demo"

const rest_events_path = process.env.REST_EVENTS_PATH || "./"
const use_rest = process.env.USE_REST || false

const slack_events_path = process.env.REST_EVENTS_PATH || "./"
const use_slack = process.env.USE_SLACK || false

const rocketchat_events_path = process.env.ROCKETCHAT_EVENTS_PATH || "./"
const use_rocketchat = process.env.USE_ROCKETCHAT || false

const gitlab_events_path = process.env.REST_EVENTS_PATH || "./"
const use_gitlab = process.env.USE_GITLAB || false

module.exports =  (app, bot) =>  {
  
  // ----- Slack Integration -----
  if (use_slack) {
    require(`./slack/initialize.js`)(app, bot)
  } else {
    console.log("🤖 > no Slack integration")
  }

  // ----- REST Integration -----
  if (use_rest) {
    require(`./rest/initialize.js`)(app, bot)
  } else {
    console.log("🤖 > no REST integration")
  }

  // ----- RocketChat Integration -----
  if (use_rocketchat) {
    require(`./rocketchat/initialize.js`)(app, bot)
  } else {
    console.log("🤖 > no RocketChat integration")
  }

  // ----- GitLab Integration -----
  if (use_gitlab) {
    require(`./gitlab/initialize.js`)(app, bot)
  } else {
    console.log("🤖 > no GitLab integration")
  }

}
