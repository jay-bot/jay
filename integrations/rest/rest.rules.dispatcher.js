console.log("🤖 > loading 🌍 REST rules")

const rest_events_path = process.env.REST_EVENTS_PATH || "./"
const whenRestEvent = require(`${rest_events_path}/when.rest.event.js`)

module.exports =  ({bot, restEvent}) =>  {
  
  restEvent.when({
    RestMessage: payload => { 
      whenRestEvent({bot: bot, event: restEvent})
      //bot.httpResponse("ok")
    },      
    _: () => { // otherwise
      console.log("😡 ouch something went bad")
      bot.httpResponse("😡 ouch something went bad")
    }
  })
  
}
