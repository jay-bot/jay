// 🌍 REST
console.log("🤖 > REST events management ✅")

const DictionnaryFromYAML = require('../../core/models/DictionnaryFromYAML').DictionnaryFromYAML

const rules_path = `./rules/${process.env.RULES_PATH}` || "./rules/demo"


// Load dictionnary(ies)
const yamlDictionnary = DictionnaryFromYAML.of(`${rules_path}/.rules.yml`)

const nlpMethod = yamlDictionnary.getNLP() 
  ? yamlDictionnary.getNLP().method 
  : "jaroWinkler" // default value
/* Possible values:

- jaroWinkler
- levenshtein
- diceCoefficient

*/

const similarityTrigger = yamlDictionnary.getNLP()
  ? yamlDictionnary.getNLP().similarityTrigger
  : 0.8

module.exports =  ({bot, event}) =>  {

  let options = {
    BOT_NAME: bot.userName(), 
    USER_NAME: event.userName(), 
    HELLO: "👋 hello 🌍 world", // this is a test
    CURRENT_PATH: rules_path,
    QUESTION: event.text() // get the question of the user
  }

  let disparities = []
  let answers = []

  bot.hearing({content: event.text()}).when({

    Notification: text => {
      options.notification=true

      let search = text.search({
        dictionnary: yamlDictionnary, 
        bot: bot, 
        event: event, 
        options: options
      })

      if(event.text().includes("%CMD")) {

        search["startsWith"]().when({
          Disparities: disparities => {},
          Similarities: similarities => {
            if(similarities.first().scriptResult) {
              answers.push(similarities.first().scriptResult)
            } else {
              answers.push(similarities.first().answer) 
            }
          }
        })

      } else {
        // Jaro-Winkler
        // best similarity -> 1
        // worst similarity -> 0
        search[nlpMethod](similarityTrigger).when({
          Disparities: disparities => {},
          Similarities: similarities => {
            if(similarities.first().scriptResult) {
              answers.push(similarities.first().scriptResult)
            } else {
              answers.push(similarities.first().answer) 
            }
          }
        })

      }

      // answering ...
      answers.length>0 
        ? bot.httpResponse({content: answers.join("\n")})    
        : bot.httpResponse({content: `😢 sorry @${event.userName()}, can you repeat please?`})  
        //TODO: read the message from yaml file
    },
    NotNotification: text => { 
      // there is a message, but it's not for the bot
      options.notification=false

      let search = text.search({
        dictionnary: yamlDictionnary, 
        bot: bot, 
        event: event, 
        options: options
      })

      search[nlpMethod](similarityTrigger).when({
        Disparities: disparities => {},
        Similarities: similarities => {
          if(similarities.first().scriptResult) {
            answers.push(similarities.first().scriptResult)
          } else {
            answers.push(similarities.first().answer) 
          }
        }
      })      

      // answering ...
      answers.length>0 
        ? bot.httpResponse({content: answers.join("\n")}) 
        : bot.httpResponse({content: `😢 sorry @${event.userName()}, can you repeat please?`})  
   
        //: null // don't add message: no notification

      //bot.httpResponse({ error: text })  
    }
  })

}
