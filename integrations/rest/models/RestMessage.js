const Egg = require('../../../core/libs/eggs').Egg

class RestMessage extends Egg {

  /* --- helpers --- */

  text() {
    return this.value().text
  }

  userName() {
    return this.value().userName || "John Doe"
  }

}

module.exports = {
  RestMessage: RestMessage
}