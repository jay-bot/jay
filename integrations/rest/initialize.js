const axios = require("axios")
console.log("🤖 > Loading 🌍 REST integration")

module.exports =  (app, bot) =>  {

  let RestMessage = require(`./models/RestMessage`).RestMessage
  const restRulesDispatcher = require(`./rest.rules.dispatcher.js`)

  /*
  ### Direct notifications

  - eg from GitLab CI or something else
  */

  /*
  curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"text":"hi @babs can you say hello","userName":"k33g"}' \
    http://jops.test:9090/rest/knock-knock

  curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"text":"hi @babs how are you","userName":"k33g"}' \
    http://jops.test:9090/rest/knock-knock

  curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"text":"hi @babs hello world","userName":"k33g"}' \
    http://jops.test:9090/rest/knock-knock

  curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"text":"hi hello world","userName":"k33g"}' \
    http://jops.test:9090/rest/knock-knock
  */
  app.post('/rest/knock-knock', (req, res) => {

    bot.setHttpContext({
      request: req,
      response: res
    })

    restRulesDispatcher({
      bot: bot,
      restEvent: RestMessage.of(req.body)
    })

  })

}



