/*
### GitLab integration
*/
//const axios = require("axios")
console.log("🤖 > Loading 🦊 GitLab integration")

const GitLabUser = require('./models/User').GitLabUser
const GitLabClient = require("./libs/gitlab-client").GitLabClient

const botGitLabToken = process.env.BOT_GITLAB_TOKEN || "b7nqJQPQwTY8sybBYJNk" // demo token
const gitLabInstanceUrl = process.env.GITLAB_URL || "http://gitlab-demo.test" // demo instance

const gitLabClient = new GitLabClient({
  baseUri: `${gitLabInstanceUrl}/api/v4`,
  token: botGitLabToken
})

/*
## GitLab events

- This map contains the GitLab events known by the bot
- If you want to add events see `./documentation/events.ADD.GITLAB.EVENT.md`
*/
let mapGitLabEvent = new Map()

mapGitLabEvent.set("issue", require(`./models/Issue`).GitLabIssue)
mapGitLabEvent.set("note", require(`./models/Note`).GitLabNote)
mapGitLabEvent.set("push", require(`./models/Push`).GitLabPush)
mapGitLabEvent.set("merge_request", require(`./models/MergeRequest`).GitLabMergeRequest)
mapGitLabEvent.set("unknown_gitlab_event", require(`./models/UnknownGitLabEvent`).UnknownGitLabEvent)

const gitLabRulesDispatcher = require(`./gitlab.rules.dispatcher.js`)

module.exports =  (app, bot) =>  {

  bot.setProperties({
    gitLabClient: gitLabClient
  })

  bot.setMethods({
    gitlab: function () {
      return this.gitLabClient
    },
    addGitLabNote: function({issue, mergeRequest, note, content}) {
      // add a note from an issue
      if(issue) {
        if(this.userName() != issue.userName()) { // else, it's the bot that responds
          let projectId = issue.projectId()
          let issueInternalId = issue.internalId()
          let body = content

          return this.gitLabClient.addNoteIssue({
            projectId, issueInternalId, body
          }).catch(error => {
            let message = `😡 [core/models/Bot.js] : ${error}`
            console.log(message)
            return message        
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve(`🤔 [core/models/Bot.js] bot cannot talk to himself - avoid infinite loop`)
            //reject(`😡 [core/models/Bot.js] bot cannot talk to himself - avoid infinite loop`)
          })
        }
      }

      // add a note from a merge rquest
      if(mergeRequest) {

        console.log("🦊 > this is a merge request")
        if(this.userName() != mergeRequest.userName()) { // else, it's the bot that responds
          let projectId = mergeRequest.projectId()
          let mergeRequestInternalId = mergeRequest.internalId()
          let body = content

          console.log("🦊 > projectId", projectId)
          console.log("🦊 > mergeRequestInternalId", mergeRequestInternalId)

          return this.gitLabClient.addNoteMergeRequest({
            projectId, mergeRequestInternalId, body
          }).catch(error => {
            let message = `😡 [core/models/Bot.js] : ${error}`
            return message        
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve(`🤔 [core/models/Bot.js] bot cannot talk to himself - avoid infinite loop`)
            //reject(`😡 [core/models/Bot.js] bot cannot talk to himself - avoid infinite loop`)
          })
        }
      }

      // add a note from a note
      if(note) {
        console.log("🎃", note)
        // a MergeRequest is an issue

        if(this.userName() != note.userName()) {
          let projectId = note.projectId()
          let body = content

          if(note.issueInternalId()) {


            let issueInternalId = note.issueInternalId()

            console.log("🎃 issueInternalId", issueInternalId)
            
            return this.gitLabClient.addNoteIssue({
              projectId, issueInternalId, body
            }).catch(error => {
              let message = `😡 [core/models/Bot.js] : ${error}`

              consolr.log(message)
              return message        
            })
          }

          if(note.mergeRequestInternalId()) {
            let mergeRequestInternalId = note.mergeRequestInternalId()
            
            return this.gitLabClient.addNoteMergeRequest({
              projectId, mergeRequestInternalId, body
            }).catch(error => {
              let message = `😡 [core/models/Bot.js] : ${error}`
              return message        
            })
          }


        } else {
          /* ... */ 
          return new Promise((resolve, reject) => {
            resolve(`🤔 [core/models/Bot.js] bot cannot talk to himself - avoid infinite loop`)
            //reject(`😡 [core/models/Bot.js] bot cannot talk to himself - avoid infinite loop`)
          })
        }

      } else {
        throw new Error(`😡 [addGitLabNote], this is not an issue, nor a note`)
      }      
    },
    hello: function() { return "hello" }
  })


  /*
  ## GitLab event "reverse proxy"

  Called by a GitLab event

  - check X-Gitlab-Token HTTP header
  http://jops.test:9090/tanuki/knock-knock

  */

  app.post('/tanuki/knock-knock', (req, res) => { // this is only for the GitLab part
    
    let object_kind = req.body.object_kind
    let event_type = req.body.event_type

    console.log("👋 [index.js] object_kind ==>", object_kind)
    
    // get the appropriate library
    const knownEvent = mapGitLabEvent.get(object_kind)

    bot.setHttpContext({
      request: req,
      response: res
    })

    if(knownEvent) {
      console.log("🎃 sknownEvent")
      gitLabRulesDispatcher({
        bot: bot,
        gitLabEvent: knownEvent.of(req.body)
      })

    } else {

      let message = `😡 [index.js] object: ${object_kind} not registered | event: ${event_type}`
      console.log(message)

      gitLabRulesDispatcher({
        bot: bot,
        gitLabEvent: mapGitLabEvent.get("unknown_gitlab_event").of(req.body)
      })
      
    }
    
  })

}



