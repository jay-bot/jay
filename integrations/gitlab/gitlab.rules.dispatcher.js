console.log("🤖 > loading 🦊 GitLab rules")

const gitlab_events_path = process.env.GITLAB_EVENTS_PATH || "./"
const whenGitLabEvent = require(`${gitlab_events_path}/when.gitlab.event.js`)

module.exports =  ({bot, gitLabEvent}) =>  {

  console.log("🔴 file: gitlab.rules.dispatcher.js line: 8")

  console.log(gitLabEvent)
  
  gitLabEvent.when({
    GitLabIssue: payload => { 
      whenGitLabEvent({bot: bot, gitLabEvent: gitLabEvent, kind: "issue"})
      bot.httpResponse("ok")
    },      
    GitLabNote: payload => {
      whenGitLabEvent({bot: bot, gitLabEvent: gitLabEvent, kind: "note"})
      bot.httpResponse("ok")
    },
    GitLabMergeRequest: payload => {
      whenGitLabEvent({bot: bot, gitLabEvent: gitLabEvent, kind: "merge_request"})
      bot.httpResponse("ok")
    },
    GitLabPush: payload => {
      whenGitLabEvent({bot: bot, gitLabEvent: gitLabEvent, kind: "push"})
      bot.httpResponse("ok")
    },
    UnknownGitLabEvent: payload => {
      whenGitLabEvent({bot: bot, gitLabEvent: gitLabEvent, kind: "other"})
      bot.httpResponse("ok")
    },
    _: () => { // otherwise
      console.log("😡 ouch something went bad")
      bot.httpResponse("😡 ouch something went bad")
    }
  })
  
}
