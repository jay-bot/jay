// 🦊 GitLab Integration
console.log("🤖 > 🦊 GitLab events management ✅")

const DictionnaryFromYAML = require('../../core/models/DictionnaryFromYAML').DictionnaryFromYAML
const rules_path = `./rules/${process.env.RULES_PATH}` || "./rules/demo"
// Load dictionnary(ies)
const yamlDictionnary = DictionnaryFromYAML.of(`${rules_path}/.rules.yml`)

const nlpMethod = yamlDictionnary.getNLP() 
  ? yamlDictionnary.getNLP().method 
  : "jaroWinkler" // default value
/* Possible values:
- jaroWinkler
- levenshtein
- diceCoefficient
*/

const similarityTrigger = yamlDictionnary.getNLP()
  ? yamlDictionnary.getNLP().similarityTrigger
  : 0.8

module.exports =  ({bot, gitLabEvent, kind}) =>  {
  console.log("🔴 file: when.gitlab.event.js line: 23")
  // kind is set in gitlab.rules.dispatcher.js
  console.log("🦊 kind",kind)
  console.log("🦊 gitLabEvent",gitLabEvent)

  //TODO: add the status of the issue (si Issue.js)
  //! in Dictionnary.parse -> add a filter (status: closed)
  //! her in options add the status of the issue
  //! 👋 add all the informations about the issue

  var options = {
    HELLO: "🚀 this is a test",
    BOT_NAME: bot.userName(), 
    USER_NAME: gitLabEvent.userName(), 
    CURRENT_PATH: rules_path,
    QUESTION: gitLabEvent.text(),
    PROJECT_ID: gitLabEvent.project() !=undefined ? gitLabEvent.project().id : null,
    PROJECT_NAME: gitLabEvent.project() !=undefined ? gitLabEvent.project().name : null,
    PROJECT_URL: gitLabEvent.project() !=undefined ? gitLabEvent.project().web_url : null
  }
  //TODO: add some properties to options
  //  - when this is a MR, add the MR ID?
  //  - add issue ID, title of the issue etc ....
  //  - add project ID
  //  - etc...
  //  - issue state `state()`

  if((kind=="note" || kind=="merge_request" || kind=="issue" || kind=="push")  && gitLabEvent.userName()!== bot.userName()) {

    let disparities = []
    let errors = []
    let answers = []
    console.log("🔴 file: when.gitlab.event.js line: 45, gitLabEvent.text()", gitLabEvent.text())
    bot.hearing({content: gitLabEvent.text()}).when({
      Notification: text => { //! this is a notification because there is the name(handle) prefixed in the text of the event
        console.log("🔴 file: when.gitlab.event.js line: 48 (this is a notification)")
        options.notification=true
        options.kind = kind

        let search = text.search({
          dictionnary: yamlDictionnary, 
          bot: bot, 
          event: gitLabEvent, 
          options: options,
          kind: kind
        })

        if(gitLabEvent.text().includes("%CMD")) {
          console.log("🍉 command command")
          search["startsWith"]().when({
            Disparities: disparities => {},
            Similarities: similarities => {
              if(similarities.first().scriptResult) {
                answers.push(similarities.first().scriptResult)
              } else {
                answers.push(similarities.first().answer) 
              }
            }
          })          
        } else {
          search[nlpMethod](similarityTrigger).when({
            Disparities: disparities => {},
            Similarities: similarities => {
              if(similarities.first().scriptResult) {
                answers.push(similarities.first().scriptResult)
              } else {
                answers.push(similarities.first().answer) 
              }
            }
          })        
        }

        //TODO: add the "merge_request" case
        switch(kind) {
          case "issue":
            answers.length>0 
              ? bot.addGitLabNote({ // Add a note to the issue
                  issue: gitLabEvent, 
                  content: answers.join("\n")
                })    
              : bot.addGitLabNote({
                  issue: gitLabEvent, 
                  content: yamlDictionnary.getErrorMessage({data: options})
                  //`😢 sorry @${gitLabIssue.userName()}, can you repeat please?`
                  //TODO add default message
                }) 
            break;
          case "note":
            answers.length>0 
              ? bot.addGitLabNote({ // Add a note to the note
                  note: gitLabEvent, 
                  content: answers.join("\n")
                })    
              : bot.addGitLabNote({
                  note: gitLabEvent, 
                  content: yamlDictionnary.getErrorMessage({data: options})
                  //`😢 sorry @${gitLabNote.userName()}, can you repeat please?`
                  //TODO add default message
                })  
            break;
          default:
            console.log("🚧 work in progress")
        }


      },
      // check where this change, becaause it updates `options`
      // 🦊 👋 There is a message but the bot is not notified with @babs
      NotNotification: text => { 
        console.log("🔴 file: when.gitlab.event.js line: 125 (😡 this is NOT a notification)")
        //console.log(text) 
        options.notification=false
        options.kind = kind

        let search = text.search({
          dictionnary: yamlDictionnary, 
          bot: bot, 
          event: gitLabEvent, 
          options: options,
          kind: kind
        })

        console.log("🔴 file: when.gitlab.event.js line: 138, before search")

        search[nlpMethod](similarityTrigger).when({
          Disparities: disparities => {},
          Similarities: similarities => {
            if(similarities.first().scriptResult) {
              answers.push(similarities.first().scriptResult)
            } else {
              answers.push(similarities.first().answer) 
            }
          }
        })    

        console.log("🔴 file: when.gitlab.event.js line: 151, answer", answers)

        //TODO: add the "merge_request" case
        switch(kind) {
          case "issue":
            answers.length>0 
              ? bot.addGitLabNote({ // Add a note to the issue
                  issue: gitLabEvent, 
                  content: answers.join("\n")
                })    
              : null // don't add message: no notification
            break;
          case "note":
            answers.length>0 
              ? bot.addGitLabNote({ // Add a note to the note
                  note: gitLabEvent, 
                  content: answers.join("\n")
                })    
              : null // don't add message: no notification
            break;
          default:
            console.log("🚧 work in progress")
        }


      }// there is a message, but it's not for the bot
    })

  } // end of note or issue or MR



}

