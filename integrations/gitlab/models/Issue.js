const Egg = require('../../../core/libs/eggs').Egg

class GitLabIssue extends Egg {

  repository() {
    return this.value().repository
  }

  object_attributes() {
    return this.value().object_attributes
  }

  assignees() {
    return this.value().assignees
  }

  assignees() {
    return this.value().labels
  }

  user() {
    return this.value().user
  }

  project() {
    return this.value().project
  }

  /* --- helpers --- */
  title() {
    return this.value().object_attributes.title
  }

  state() {
    return this.value().object_attributes.state
  }

  description() {
    return this.value().object_attributes.description
  }

  text() {
    return this.value().object_attributes.description
  }

  userName() {
    return this.value().user.username // handle
  }

  internalId() {
    return this.value().object_attributes.iid
  }

  projectId() {
    return this.value().project.id
  }

}

module.exports = {
  GitLabIssue: GitLabIssue
}