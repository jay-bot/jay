const Egg = require('../../../core/libs/eggs').Egg

class GitLabPush extends Egg {

  userName() {
    //console.log(this.value())
    return this.value().user_username // handle
  }

  handle() {
    return this.value().user_username // handle
  }

  completeUserName() {
    return this.value().user_name // handle
  }

  text() {
    // commit message
    console.log("🔴 file: Push.js line: 12, text(), commit message:", this.value().message)

    if(this.value().message==null || this.value().message==undefined) {
      return ""
    } else {
      return this.value().message
    }
    
  }

  message() {
    // commit message
    return this.value().message
  }


  project() {
    return this.value().project
  }

  //TODO: 🚧 WIP

}

module.exports = {
  GitLabPush: GitLabPush
}

/*
{ object_kind: 'push',
  event_name: 'push',
  before: 'ad5d257353028fe0552bb31e2876b7344e0eb779',
  after: '5c7777415feba8035a838624449b456717820f6e',
  ref: 'refs/heads/master',
  checkout_sha: '5c7777415feba8035a838624449b456717820f6e',
  message: null,
  user_id: 841428,
  user_name: 'Philippe Charrière',
  user_username: 'k33g',
  user_email: '',
  user_avatar:
   'https://secure.gravatar.com/avatar/a4c2abd4ac3d265b4e04e14d26187e84?s=80&d=identicon',
  project_id: 13195595,
  project:
   { id: 13195595,
     name: 'talk-to-jaybot',
     description: '',
     web_url: 'https://gitlab.com/k33g/talk-to-jaybot',
     avatar_url: null,
     git_ssh_url: 'git@gitlab.com:k33g/talk-to-jaybot.git',
     git_http_url: 'https://gitlab.com/k33g/talk-to-jaybot.git',
     namespace: 'k33g',
     visibility_level: 20,
     path_with_namespace: 'k33g/talk-to-jaybot',
     default_branch: 'master',
     ci_config_path: null,
     homepage: 'https://gitlab.com/k33g/talk-to-jaybot',
     url: 'git@gitlab.com:k33g/talk-to-jaybot.git',
     ssh_url: 'git@gitlab.com:k33g/talk-to-jaybot.git',
     http_url: 'https://gitlab.com/k33g/talk-to-jaybot.git' },
  commits:
   [ { id: '5c7777415feba8035a838624449b456717820f6e',
       message: 'Update README.md',
       timestamp: '2019-07-08T13:37:09Z',
       url:
        'https://gitlab.com/k33g/talk-to-jaybot/commit/5c7777415feba8035a838624449b456717820f6e',
       author: [Object],
       added: [],
       modified: [Array],
       removed: [] } ],
  total_commits_count: 1,
  push_options: {},
  repository:
   { name: 'talk-to-jaybot',
     url: 'git@gitlab.com:k33g/talk-to-jaybot.git',
     description: '',
     homepage: 'https://gitlab.com/k33g/talk-to-jaybot',
     git_http_url: 'https://gitlab.com/k33g/talk-to-jaybot.git',
     git_ssh_url: 'git@gitlab.com:k33g/talk-to-jaybot.git',
     visibility_level: 20 } }



*/