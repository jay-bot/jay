const Egg = require('../../../core/libs/eggs').Egg


class GitLabNote extends Egg {
  issue() {
    return this.value().issue
  }
  repository() {
    return this.value().repository
  }

  object_attributes() {
    return this.value().object_attributes
  }

  user() {
    return this.value().user
  }

  project() {
    return this.value().project
  }

  /* --- helpers --- */

  text() {
    return this.value().object_attributes.note
  }

  note() {
    return this.value().object_attributes.note
  }

  userName() {
    return this.value().user.username // handle
  }

  issueInternalId() {
    if(this.value().issue) {
      return this.value().issue.iid
    } else {
      return undefined
    }
  }

  mergeRequestInternalId() {
    if(this.value().merge_request) {
      return this.value().merge_request.iid
    } else {
      return undefined
    }    
  }

  noteableId() {
    return this.value().object_attributes.noteable_id
  }


  projectId() {
    return this.value().project.id
  }


}

module.exports = {
  GitLabNote: GitLabNote
}