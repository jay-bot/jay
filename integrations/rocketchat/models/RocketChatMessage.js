const Egg = require('../../../core/libs/eggs').Egg

/*
{ token: 'w9raijFRzs3ofEvCx9r6yvcL',
  bot: false,
  trigger_word: '@babs',
  channel_id: 'GENERAL',
  channel_name: 'general',
  message_id: 'pjCAejMAskECNTMip',
  timestamp: '2019-02-22T10:56:07.660Z',
  user_id: 'st9sRWJg4fvgEHJLx',
  user_name: 'k33g',
  text: '@babs hello' }
*/


class RocketChatMessage extends Egg {

  /* --- helpers --- */

  text() {
    return this.value().text
  }

  bot() {
    return this.value().bot
  }

  token() {
    return this.value().token
  }

  triggerWord() {
    return this.value().trigger_word
  }


  channelId() {
    return this.value().channel_id
  }

  channelName() {
    return this.value().channel_name
  }

  messageId() {
    return this.value().message_id || "???"
  }

  userId() {
    return this.value().user_id || "???"
  }
  userName() {
    return this.value().user_name || "John Doe"
  }

}

module.exports = {
  RocketChatMessage: RocketChatMessage
}