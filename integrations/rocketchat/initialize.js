const axios = require("axios")
console.log("🤖 > Loading 🚀 🐱 RocketChat integration")

module.exports =  (app, bot) =>  {
  let RocketChatMessage = require(`./models/RocketChatMessage`).RocketChatMessage
  const rocketChatRulesDispatcher = require(`./rocketchat.rules.dispatcher.js`)

  /*
  ## RocketChat configuration
  > example:
  ```
  ROCKETCHAT_HOOKS_BASE_URL="http://rocketchat.test:3000/hooks/"
  ROCKETCHAT_CHANNELS="general,random"
  ROCKETCHAT_INCOMING_TOKENS="mvsMyHNEnRNq3ND4E/q7nBnv9MY69oJBgCYcZS7Rmpom2AuoihNjidf2CWsYwDvvpX,99aZfw5RiBE2mKCqy/GeHyQTd4eNhvLAwer9AwdS8fPMXGTBjNiTczxksQ4Z9vfycb"
  ROCKETCHAT_OUTGOING_TOKENS="w9raijFRzs3ofEvCx9r6yvcL,NQjQdbL9i6QbA7mPKCWbgF8M"
  ```
  */
  let rocketChatHooksBaseUrl = process.env.ROCKETCHAT_HOOKS_BASE_URL || null
  let rocketChatChannels = process.env.ROCKETCHAT_CHANNELS.split(",")
  let rocketChatIncomingTokens = process.env.ROCKETCHAT_INCOMING_TOKENS.split(",")
  let rocketChatOutgoingTokens = process.env.ROCKETCHAT_OUTGOING_TOKENS.split(",")

  let rocketChatWebHooks = new Map()

  rocketChatChannels.forEach(channel => {
    rocketChatWebHooks.set(
      channel, 
      {
        hook:rocketChatHooksBaseUrl+rocketChatIncomingTokens[rocketChatChannels.indexOf(channel)],
        token:rocketChatOutgoingTokens[rocketChatChannels.indexOf(channel)]
      }
    )
  })

  console.log("🚀 RocketChat webhooks:", rocketChatWebHooks)

  bot.setProperties({
    rocketChatWebHooks: rocketChatWebHooks
  })

  bot.setMethods({
    sendToRocketChat: function(channel, message) {
      let webHook = this.rocketChatWebHooks.get(channel).hook

      return axios({
        method: "POST",
        url: webHook,
        headers: {"Content-Type": "application/json"},
        data: JSON.stringify({"text": message})
      })
      /*
      .then(response => response)
      .catch(err => console.log(`😡 Context.send`, err))
      */    
    }
  })

  // to do setMethod

  /*
  ### RocketChat integration
  */
  app.post('/rocketchat/knock-knock', (req, res) => {
    
    let message = RocketChatMessage.of(req.body)
    console.log("🚀 rocket chat knock knock from:", message.channelName())

    let rocketChatBotToken = rocketChatWebHooks.get(message.channelName()).token

    bot.setHttpContext({
      request: req,
      response: res
    })

    if(message.token() == rocketChatBotToken && !message.bot()) {
      //console.log("from:", message.userName())
      //console.log("message:", message.text())
      rocketChatRulesDispatcher({
        bot: bot,
        rocketChatEvent: message
      })

      res.status(200).end()

    } else {
      console.log("😡 🚀 rocket chat knock knock with bad token")
      res.status(200).end()
    }

    
  })

  //TODO: add token? or not
  //TODO: to be secured
  app.post('/rocketchat/message', (req, res) => {
    console.log("🐠 [RocketChat]Message from the API", req.body)
    bot.sendToRocketChat(req.body.channel, req.body.text)
    res.status(200).end()
  })


}



