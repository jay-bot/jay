console.log("🤖 > loading 🚀 🐱 RocketChat rules")

const rocketchat_events_path = process.env.REST_EVENTS_PATH || "./"
const whenRocketChatEvent = require(`${rocketchat_events_path}/when.rocketchat.event.js`)

module.exports =  ({bot, rocketChatEvent}) =>  {
  
  rocketChatEvent.when({
    RocketChatMessage: payload => { 
      whenRocketChatEvent({bot: bot, event: rocketChatEvent})
      bot.httpStatus(200)
    },      
    _: () => { // otherwise
      let message = "😡 ouch something went bad"
      console.log(message)
      bot.httpResponse(message)
    }
  })
  
}
