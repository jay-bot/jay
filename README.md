# Jay

> ⚠️ this a WIP 🚧

## What is Jay?

- Jay is part of the family of Jay-Bot group. Jay is a "devops" bot and you set up him with a yaml file
- You can chat with Jay through REST, Slack, RocketChat, GitLab self-hosted managed (with issues, notes and merge requests) at the same time

## How to install and run Jay

### Install

- Clone this repository
- Run `npm install`

### Create a rules directory

- Create a directory to host your rules in the `./rules` directory
- The name of the rules' file must be: `.rules.yml`
- There is a sample in `./rules/demo`

#### Rules sample

```yaml
name: "my-rules"
version: 0.0.0

# this is used when Jay cannot understant what you mean
errorMessage: "😢 sorry @${data.USER_NAME}, I don't understand, can you repeat please?"

# choose the natural language processing you prefer
nlp: 
  method: "jaroWinkler"
  similarityTrigger: 0.8

initialize: | # run it before each script
  # intialize some variables and functions
  GREETING="👋 Hello World 🌍"
  say_hello() {
    echo "$GREETING"
  }
  # variables and functions are intialized

rules:  
  ping:
    sentence: "@${data.BOT_NAME} do you want to play ping pong with me?"
    answer: "🏓 pong ${data.USER_NAME}"

  # you can use `script` instead of `answer`
  emojis:
    only: note
    sentence: "@${data.BOT_NAME} git emoji"
    script: |
      doc=`cat ${data.CURRENT_PATH}/gitmoji.md`
      echo "${doc}"

  awesome:
    sentence: "this is amazing"
    handle: empty 
    script: |
      echo "🎃 yes it is ✨"
```

### Run Jay

You need to set up some environment variables and run `npm start`: 

```shell 
#!/bin/sh
PORT=8080 \
USE_GITLAB=true \
USE_SLACK=true \
USE_ROCKETCHAT=true \
USE_REST=true \
RULES_PATH="demo" \
MESSAGE="Hello World, my name is jaybot" \
BOT_GITLAB_TOKEN=gitlab_token_of_the_bot_user \
BOT_NAME=jaybot \
GITLAB_URL=https://gitlab.com \
SLACK_HOOKS_BASE_URL="https://hooks.slack.com/services/" \
SLACK_INCOMING_TOKEN="incoming_slack_token" \
SLACK_CHANNELS="general,demo" \
SLACK_OUTGOING_TOKENS="outgoing_general_slack_token,outgoing_demo_slack_token" \
ROCKETCHAT_HOOKS_BASE_URL="http://rocketchat.test:3000/hooks/" \
ROCKETCHAT_CHANNELS="general" \
ROCKETCHAT_INCOMING_TOKENS="incoming_rocketchat_token" \
ROCKETCHAT_OUTGOING_TOKENS="outgoing_general_rocketchat_token" \
npm start
```

## Use it with GitLab

> 🚧 work in progress

### On the GitLab side

- Go to the admin panel `/admin/application_settings/network` and expand the **Outbound requests** section
  - check **Allow requests to the local network from hooks and services**
  - save changes
- Create a group or a project and add a hook group or hook project (*Settings/Webhooks*)
  - with this url [http://your-bot-domain:port/knock-knock](http://your-bot-domain:port/knock-knock)
    - eg: [http://jay.test:9090/knock-knock](http://jay.test:9090/knock-knock)
  - use a **Secret Token** (or not)
  - check all triggers
- Create a project

#### Create a "bot" user

- create a user, eg: `babs`
- generate a token for this user, eg: `Jv2vXsJBWqoLbMmpHaxU`
- start the project like that: `BOT_GITLAB_TOKEN=Jv2vXsJBWqoLbMmpHaxU BOT_NAME=babs npm start`

> **Remark**: the bot user must be a member of your group, projects ...


## Use it with REST call

> 🚧 work in progress


## Use it with Rocket Chat

### Set up

#### Admin setup

- connect as root
- go to **Administration** section
- create a new user with a name (eg:`babs`) (add an email, set `Verified` to `true`, add the `bot` role, set `Join default channels` to `true`)
- connect to the user bot to set an avatar

Then,

- go to **Integrations** section
- click on **New Integration** button

#### Integrations

##### Incoming Webhook (when the bot answers)

- click on **Incoming Webhook**
- set `Enabled` to `true`
- add a `Channel` (eg *#general*)
- set the username (eg *babs*) in `Post as` field *(The user must already exist)*
- copy the webhook base url and setup the environment variable `ROCKETCHAT_HOOKS_BASE_URL` with it (eg: `ROCKETCHAT_HOOKS_BASE_URL="http://rocketchat.test:3000/hooks/"`)
- copy the webhook token and setup the environment variable `ROCKETCHAT_INCOMING_TOKENS` with it (eg: `ROCKETCHAT_INCOMING_TOKENS="mvsMyHNEnRNq3ND4E/q7nBnv9MY69oJBgCYcZS7Rmpom2AuoihNjidf2CWsYwDvvpX,99aZfw5RiBE2mKCqy/GeHyQTd4eNhvLAwer9AwdS8fPMXGTBjNiTczxksQ4Z9vfycb"`)
  - separate the tokens by `,` if you use several channels
- copy the channel and setup the environment variable `ROCKETCHAT_CHANNELS` with it (eg: `ROCKETCHAT_CHANNELS="general,random"`)
  - separate the channels by `,` if you use several channels
  - the order of channels must follow the order of the tokens
- click on the **Save Changes** button

##### Outgoing Webhook (when you ask something to the bot)

- click on **Outgoing Webhook**
- choose the `Event Trigger`: *Message Sent*
- set `Enabled` to `true`
- add a `Channel` (eg *#general*)
- add `Trigger Words ` (eg *@babs*)
- add a `Callback URL` (http://bot_domain:port/rocketchat/knock-knock) *the url always ends with `rocketchat/knock-knock`*
- set `Impersonate User` to `true`
- set the environment variable `ROCKETCHAT_OUTGOING_TOKENS` with the given token (eg: `ROCKETCHAT_OUTGOING_TOKENS="w9raijFRzs3ofEvCx9r6yvcL,NQjQdbL9i6QbA7mPKCWbgF8M"`)
  - separate the tokens by `,` if you use several channels
  - the order of tokens must follow the order of the channels
- click on the **Save Changes** button


## Use it with Slack

### Slack setup

- Go to `Administration > Manage Apps > Custom Integration`

### Incoming Webhook

> It allows the bot to post to Slack

- go to **Incoming Webhook**
- click on **Add Configuration**
- select a `Channel` (eg *random*, you can change the channel by code) 
- click on **Add Incoming Webhook**
- use the webhook url and token to setup `SLACK_HOOKS_BASE_URL` and `SLACK_INCOMING_TOKEN`

### Outgoing Webhook

> It allows to speak to the bot

- go to **Outgoing Webhook**
- click on **Add Configuration**
- click on **Add Outgoing Webhook**
- select *a channel* for `Channel` ⚠️ you need to create several Outgoing integrations if you want to use several channels
- add `Trigger Words ` (eg *@tod*)
- add a `Callback URL` (http://bot_domain:port/slack/knock-knock) *the url always ends with `slack/knock-knock`*
- set the environment variables like that:

```shell
SLACK_CHANNELS="general,random" \
SLACK_OUTGOING_TOKENS="AAAAA,BBBBB" \
```

> the order of the outgoing tokens must follow the order of the channels

