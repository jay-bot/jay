_An emoji guide for your commit messages_
> - Improving structure / format of the code: 🎨
> - Fixing a bug: 🐛
> - Deploying stuff: 🚀

See [https://gitmoji.carloscuesta.me/](https://gitmoji.carloscuesta.me/)
