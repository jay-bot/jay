const yaml = require('js-yaml')
const fs = require('fs')

const Dictionnary = require('./Dictionnary').Dictionnary

class DictionnaryFromYAML extends Dictionnary {

  rules({data}) {

    // immutability
    // var _rules = Object.assign({}, this.value().rules)
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Deep_Clone
    // 👋 deep clone
    var _rules = JSON.parse(JSON.stringify(this.value().rules))

    for(var ruleName in _rules) { 
      for(var member in data) {
        if(_rules[ruleName].sentence) {
          _rules[ruleName].sentence = _rules[ruleName].sentence.split("${data."+member+"}").join(data[member])
        } else {
          _rules[ruleName].sentence = ""
        }
        
        if(_rules[ruleName].answer) _rules[ruleName].answer = _rules[ruleName].answer.split("${data."+member+"}").join(data[member])
        if(_rules[ruleName].script) _rules[ruleName].script = _rules[ruleName].script.split("${data."+member+"}").join(data[member])

        // it should be better to have this in the integration part?
        //if(_rules[ruleName].slack) _rules[ruleName].slack = _rules[ruleName].slack.split("${data."+member+"}").join(data[member])
        //if(_rules[ruleName]. ) _rules[ruleName].rocketchat = _rules[ruleName].rocketchat.split("${data."+member+"}").join(data[member])

      }
    }
    return _rules
  }

  getErrorMessage({data}) {
    if(this.value().errorMessage) {
      var _error_message = this.value().errorMessage
      for(var member in data) {
        _error_message = _error_message.split("${data."+member+"}").join(data[member])
      }
      return _error_message        
    } else {
      return null
    }
  }

  getNLP() {
    if(this.value().nlp) {
      return this.value().nlp       
    } else {
      return null
    }
  }



  initialize({data}) {
    
    if(this.value().initialize) {
      var _initialize = this.value().initialize
      for(var member in data) {
        _initialize = _initialize.split("${data."+member+"}").join(data[member])
      }
      return _initialize      
    } else {
      //TODO add default message
      return null
    }
  }

  static of(yamlFile) {
    try {
      const content = yaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'));
      return new this(content)
    } catch(error) {
      console.log(`😡 there was a problem when loading ${yamlFile}`)
      console.log(error.name, error.message)
      //TODO: implement something here
    }    
  }
}

module.exports = {
  DictionnaryFromYAML: DictionnaryFromYAML
}





