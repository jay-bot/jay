const Egg = require('../libs/eggs').Egg
const when = require('../libs/eggs').when
const natural = require('natural')

const Failure = require('./Failure').Failure
const Success = require('./Success').Success

const Disparity = require('./Disparity').Disparity
const Similarity = require('./Similarity').Similarity
const Disparities = require('./Disparities').Disparities
const Similarities = require('./Similarities').Similarities


Array.prototype.first = function () {
    return this[0]
};

class Message extends Egg {

  search({dictionnary, bot, event, options, kind}) { // 👋 `options` is to share variables with the scripts, sentence, answer, ...
    //console.log("🔴 file: Message.js line: 188, search()")
    //console.log("🔴 file: Message.js line: 189, options:", options)
    //console.log("🔴 file: Message.js line: 190, kind:", kind)

    let message = event.text()
    let answers = []
    let disparities = []

    return {
      //! here add startsWith
      startsWith: () => {

        dictionnary.parse({data: options, job: ({ruleName, sentence, answer, action, script, only, except, handle, initialize, codeEvent}) => {
        
          if(message.startsWith(sentence)) {
            console.log("TADA 🎉")

            answers.push({
              ruleName,
              action,
              scriptResult: script ? dictionnary.run_shell_script({initialize, script}) : undefined, 
              answer: answer,
              words: ""       
            })
          } else {

            disparities.push({
              ruleName,
              action,
              message: `😡 the message does not starts with ${message}`
            })
          }

        }})

        return answers.length>0 
          ? Similarities.of(answers) 
          : Disparities.of(disparities)

      },

      // the message must contain at least one word of the sentence - no matter the order
      containsAtLeastOneWord: () => {

        dictionnary.parse({data: options, job: ({ruleName, sentence, answer, action, script, only, except, handle, initialize, codeEvent}) => {

          let result = dictionnary.tokenize_and_prepare({sentence, answer, message, options})

          var count = 0
          var wordsFound = []
          result.words.forEach(item => { if(result.tokens.includes(item)) { 
            count = count + 1 
            wordsFound.push(item)
          }})

          if(count>=1) {
            answers.push({
              ruleName,
              action,
              scriptResult: script ? dictionnary.run_shell_script({initialize, script}) : undefined, 
              answer: result.answer,
              words: result.words        
            })
          } else {

            disparities.push({
              ruleName,
              action,
              message: `😡 the message does not contains any word of ${result.words.join(",")}`
            })
          }


        }})

        return answers.length>0 
          ? Similarities.of(answers) 
          : Disparities.of(disparities)
        

      },

      // the message must contain all the words of the sentence - no matter the order
      containsAllWords: () => {

        dictionnary.parse({data: options, job: ({ruleName, sentence, answer, action, script, only, except, handle, initialize, codeEvent}) => {

          let result = dictionnary.tokenize_and_prepare({sentence, answer, message, options})

            var count = 0
            result.words.forEach(item => { if(result.tokens.includes(item)) { count = count + 1 } })

            if(count==result.words.length) {

              answers.push({
                ruleName,
                action,
                scriptResult: script ? dictionnary.run_shell_script({initialize, script}) : undefined, 
                answer: result.answer,
                words: result.words         
              })

            } else {

              disparities.push({
                ruleName,
                action,
                message: `😡 the message does not contains ${result.words.join(",")}`
              })

            }


        }})

        return answers.length>0 
          ? Similarities.of(answers) 
          : Disparities.of(disparities)
      },   


      jaroWinkler: (similarityTrigger=0.8) => {

        dictionnary.parse({data: options, kind: kind, job: ({ruleName, sentence, answer, action, script, only, except, handle, initialize, codeEvent}) => {
          
          let result = dictionnary.tokenize_and_prepare({sentence, answer, message, options})

          let jaroWinklerDistance = natural.JaroWinklerDistance(result.words.join(" "), result.message)

          if(jaroWinklerDistance > similarityTrigger) {

            answers.push({
              ruleName,
              action,
              scriptResult: script ? dictionnary.run_shell_script({initialize, script}) : undefined, 
              answer: result.answer,
              words: result.words,
              jaroWinklerDistance,
              similarityTrigger            
            })

          } else {

            disparities.push({
              ruleName,
              action,
              message:`😡 the message is too far from ${result.words.join(",")} jaroWinklerDistance: ${jaroWinklerDistance} vs similarityTrigger: ${similarityTrigger}`,
              jaroWinklerDistance,
              similarityTrigger              
            })

          }


        }})

        return answers.length>0 
          ? Similarities.of(answers) 
          : Disparities.of(disparities)
               
      },
      levenshtein: (similarityTrigger=5) => {

        dictionnary.parse({data: options, kind: kind, job: ({ruleName, sentence, answer, action, script, only, except, handle, initialize, codeEvent}) => {

          let result = dictionnary.tokenize_and_prepare({sentence, answer, message, options})

          let levenshteinDistance = natural.LevenshteinDistance(result.words.join(" "), result.message)

          if(levenshteinDistance <= similarityTrigger) {

            answers.push({
              ruleName,
              action,
              scriptResult: script ? dictionnary.run_shell_script({initialize, script}) : undefined, 
              answer: result.answer,
              words: result.words,
              levenshteinDistance,
              similarityTrigger            
            })

          } else {

            disparities.push({
              ruleName,
              action,
              message:`😡 the message is too far from ${result.words.join(",")} levenshteinDistance: ${levenshteinDistance} vs similarityTrigger: ${similarityTrigger}`,
              levenshteinDistance,
              similarityTrigger              
            })

          }

        }})

        return answers.length>0 
          ? Similarities.of(answers) 
          : Disparities.of(disparities)
      },
      diceCoefficient: (similarityTrigger=0.75) => {

        dictionnary.parse({data: options, kind: kind, job: ({ruleName, sentence, answer, action, script, only, except, handle, initialize, codeEvent}) => {

          let result = dictionnary.tokenize_and_prepare({sentence, answer, message, options})

          let diceCoefficient = natural.DiceCoefficient(result.words.join(" "), result.message)

          if(diceCoefficient > similarityTrigger) {

            answers.push({
              ruleName,
              action,
              scriptResult: script ? dictionnary.run_shell_script({initialize, script}) : undefined,
              answer: result.answer,
              words: result.words,
              diceCoefficient,
              similarityTrigger            
            })

          } else {

            disparities.push({
              ruleName,
              action,
              message:`😡 the message is too far from ${result.words.join(",")} diceCoefficient: ${diceCoefficient} vs similarityTrigger: ${similarityTrigger}`,
              diceCoefficient,
              similarityTrigger              
            })

          }

        }})    
        
        return answers.length>0 
          ? Similarities.of(answers) 
          : Disparities.of(disparities)
      } // end of diceCoefficient
    }
  }
} // end of message

module.exports = {
  Message: Message
}