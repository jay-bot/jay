const Egg = require('../libs/eggs').Egg

const shell = require('shelljs')
const natural = require('natural')

class Dictionnary extends Egg {

  dynamic() {
    return false
  }

  run_shell_script({initialize, script}) {
    var final_script = null
    if(initialize) {
      final_script = initialize + "\n" + script
    } else {
      final_script = script
    }

    const { stdout, stderr, code } = shell.exec(final_script, {silent:true})
    if(stderr) console.error(stderr, code)
    if(stdout) console.log(stdout)
    return stdout || `error: ${stderr} code: ${code}`
  }

  parse({job, kind, data}) {

    // here we parse all the rules
    // for each rules we do a research:
    // this `parse` method is call from `Messsage.js`
    // the dictionnary will compare the sentence for each rule
  
    for(var m in this.rules({data})) {

      //TODO: except
      let only = this.rules({data})[m].only
      let handle = this.rules({data})[m].handle
      let codeEvent = this.rules({data})[m].codeEvent
      let sentence = this.rules({data})[m].sentence

      // the bot has been notified or not and there is a sentence
      if(sentence.length>0) { 
        // you need to manage the only
        if(only==undefined || only.includes(kind)) {
          job({
            ruleName: m, 
            sentence: this.rules({data})[m].sentence, 
            answer: this.rules({data})[m].answer,
            action: this.rules({data})[m].action,
            script: this.rules({data})[m].script,
            only: this.rules({data})[m].only,
            except: this.rules({data})[m].except,
            handle: this.rules({data})[m].handle,
            codeEvent: this.rules({data})[m].codeEvent,
            initialize: this.initialize({data})
          })  
        }
      }

      // there is no sentence, the bot is not notified, this is a DVCS Event
      if(!data.notification && sentence.length==0 && codeEvent==data.kind) {
        this.run_shell_script({
          initialize: this.initialize({data}), 
          script: this.rules({data})[m].script
        })
      }

    }

  }

  // called from message.search
  tokenize_and_prepare({sentence, answer, message, options}) {

      var _sentence = sentence
      var _answer = answer

      let tokenizer = new natural.WordTokenizer()
      let words = tokenizer.tokenize(_sentence)
      
      let tokens = tokenizer.tokenize(message)

      return {tokens, answer: _answer, words, message}  
  }

}

module.exports = {
  Dictionnary: Dictionnary
}





