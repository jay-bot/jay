const Egg = require('../libs/eggs').Egg
const when = require('../libs/eggs').when

const Message = require('./Message').Message
const Failure = require('./Failure').Failure
const Success = require('./Success').Success
const Notification = require('./Notification').Notification
const NotNotification = require('./NotNotification').NotNotification

class Bot extends Egg {
  constructor(properties) {
    super(properties)
  }

  setHttpContext({response, request}) {
    this.response = response
    this.request = request
    return this
  }

  userName() {
    return this.value().userName
  }

  hearing({content, start}) {
    if(start) { 
      // the content must starts with a keyword (the value of start) followed by the handle of the bot, prefixed by "@"
        if(content.startsWith(`${start} @${this.userName()}`)) {
          return Notification.of(Message.of(content))
        } else {
          return NotNotification.of(Message.of(content))
          //return NotNotification.of(`👋 There is a message but the bot is not notified with ${start} @${this.userName()}`)
        }

    } else {
      // only content, content contains the handle of the bot, prefixed by "@"
      if(content.includes(`@${this.userName()}`)) {
        return Notification.of(Message.of(content))
      } else {
        return NotNotification.of(Message.of(content))
        //return NotNotification.of(`👋 There is a message but the bot is not notified with @${this.userName()}`)
      }
    }    
  }

  httpResponse(data) {
    return this.response.send(data)
  }

  httpStatus(status) {
    return this.response.status(status).end()
  }

}

module.exports = {
  Bot: Bot
}