
String.prototype.formatText = function() { return this.split("\n").map(item => item.trim()).join("\n") }

String.prototype.extractTextBetweenCurlyBraces = function() {
  let found = [], rxp = /{([^}]+)}/g, curMatch
  while(curMatch = rxp.exec(this)) {
    found.push(curMatch[1])
  }
  return found
}

String.prototype.extractTextBetweenBrackets = function() {
  let found = [], rxp = /\[(.*?)\]/g, curMatch
  while(curMatch = rxp.exec(this)) {
    found.push(curMatch[1])
  }
  return found
}

function formatText(text) { return text.split("\n").map(item => item.trim()).join("\n") }

function extractTextBetweenCurlyBraces(text) {
  let found = [], rxp = /{([^}]+)}/g, curMatch
  while(curMatch = rxp.exec(text)) {
    found.push(curMatch[1])
  }
  return found
}

function extractTextBetweenBrackets(text) {
  let found = [], rxp = /\[(.*?)\]/g, curMatch
  while(curMatch = rxp.exec(text)) {
    found.push(curMatch[1])
  }
  return found
}


module.exports = {
  formatText: formatText,
  format:formatText,
  extractTextBetweenCurlyBraces: extractTextBetweenCurlyBraces,
  extractTextBetweenBrackets: extractTextBetweenBrackets
}

